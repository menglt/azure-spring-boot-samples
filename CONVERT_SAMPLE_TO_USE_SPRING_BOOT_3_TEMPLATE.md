# 1 Convert sample to use Spring Boot 3?

## 1.1 Update your infra

- [JDK17](https://www.oracle.com/java/technologies/downloads/).

## 1.2 Update the pom parent
   
   Note: The pom parent of the samples should point to *azure-spring-boot-samples-6.x-pom.xml*.

## 1.3 Update dependencies(optional)

## 1.4 Update Java code(optional)
